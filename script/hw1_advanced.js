/*
* шоб СТВОРИТИ КЛАС, об'єкти якого описують параметри гамбургера: 
* 
* @constructor
* @param size        - розмір
* @param stuffing    - начинка
* @throws {HamburgerException} - якщо щось неправельно було зроблено
*/

/*
* щоб ПОКАЗАТИ ПОМИЛКУ в ході роботи з гамбургером:
* (саме повідомлення про помилку зберігається у властивості message)
* 
* @constructor 
*/



/* створюємо функцію-конструктор для Hamburger - (з вимогами для ES5) */

function Hamburger(size, stuffing) {
  try{
    if(!size){
      throw new HamburgerException("No size given")
    }
    if(!stuffing){
      throw new HamburgerException("No stuffing added")
    }

    // задаємо функцію перевірки та показу помилки, якщо задано замість розміру щось інше
    var flag = false;
    for(let key in Hamburger){
      if(key.toLowerCase().indexOf("size") !== -1 && size === Hamburger[key]){
        flag = true;
      }
    }
    if(!flag){
      throw new HamburgerException("invalid size")
    };

    // задаємо функцію перевірки та показу помилки, якщо задано замість начинки щось інше
    var flag = false;
    for(let key in Hamburger){
      if(key.toLowerCase().indexOf("stuffing") !== -1 && stuffing === Hamburger[key]){
        flag = true;
      }
    }
    if(!flag){
      throw new HamburgerException("invalid staffing")
    };

      
    var burgerSize = size;
    this.getSize = function () {
      return burgerSize
    };
    this.setSize = function (value) {
      return burgerSize = value
    };

    var burgerStuffing = stuffing;
    this.getStuffing = function () {
      return burgerStuffing
    };
    this.setStuffing = function (value) {
      return burgerStuffing = value
    };

    var burgerToppings = [];
    this.getBurgerToppings = function () {
      return burgerToppings;
    }
  } 
  catch (e) {
    console.log(e.message);
  }
}

/* створюємо ф-к для виведення помилки */

function HamburgerException (message) {
  this.message = message;
}

/* Визначаємо розміри, види начинок та добавок;
задаємо їм значення ціни та калорійності */

Hamburger.SIZE_SMALL = {
  price: 50,
  calories: 20
};
Hamburger.SIZE_LARGE = {
  price: 100,
  calories: 40
};
Hamburger.STUFFING_CHEESE = {
  price: 10,
  calories: 20
};
Hamburger.STUFFING_SALAD = {
  price: 20,
  calories: 5
};
Hamburger.STUFFING_POTATO = {
  price: 15,
  calories: 10
};
Hamburger.TOPPING_MAYO = {
  name: "TOPPING_MAYO",
  price: 20,
  calories: 5
};
Hamburger.TOPPING_SPICE = {
  name: "TOPPING_SPICE",
  price: 15,
  calories: 0
};

/*
* щоб ДОДАТИ ДОБАВКУ гамбургеру: 
* (можна додати декілька добавок при умові, що вони різні)
*
* @param topping     - тип добавки
* @throws {HamburgerException}  - якщо була неправельно додана
*/

Hamburger.prototype.addTopping = function (topping) {
  try {
    if (!this.getBurgerToppings().includes(topping)){ // якщо не включає добавку
      return this.getBurgerToppings().push(topping)   // тоді додати її 
    }
    if (this.getBurgerToppings().includes(topping)){
      throw new HamburgerException ("dublicate topping");
    }
    else {
      throw new HamburgerException ("No toppings added");
    }
  }
  catch (e){
    console.log(e.message);
  }
};

/*
* шоб ЗАБРАТИ ДОБАВКУ, якщо вона раніше була додана:
* 
* @param topping   - тип добавки
* @throws {HamburgerException}  - якщо була неправельно забрана
*/

Hamburger.prototype.removeTopping = function (topping) {
  try {
    if (this.getBurgerToppings().indexOf(topping)!== -1){
      return this.getBurgerToppings().splice(this.getBurgerToppings().indexOf(topping));
    }
    else {
      throw new HamburgerException ("topping is not correctly removed");
    }
  } catch (e){
    console.log(e.message);
  }
}

/*
* щоб дізнатись про ДОБАВКИ в гамбургері:
*
* @return {Array} - масив доданих добавок, який має
*                  Hamburger.TOPPING_*
*/
Hamburger.prototype.getToppings = function () {
  return this.getBurgerToppings();
};


/*
* щоб дізнатись про РОЗМІР гамбургера:
*/
Hamburger.prototype.getSize = function () {
  return this.getSize();
};

/*
 * щоб дізнатись про НАЧИНКУ гамбургера:
 */
Hamburger.prototype.getStuffing = function () {
  return this.getStuffing();
};

/*
* щоб отримати ЦІНУ гамбургера:
*
* @return {Number} - ціна в тугриках
*/

Hamburger.prototype.calculatePrice = function () {
  return this.getBurgerToppings().reduce(function(acc, prices) {return acc + prices.price}, 0)
    + this.getSize().price + this.getStuffing().price
};

/*
* щоб отримати загальну кількість КАЛОРІЇВ замовленого гамбургера:
*
* @return {Number} - калорійність в калоріях
*/

Hamburger.prototype.calculateCalories = function () {
  return this.getBurgerToppings().reduce(function(acc, calories) {return acc + calories.calories}, 0)
  + this.getSize().calories + this.getStuffing().calories
};




/*        ПЕРЕВІРКА РОБОТИ КЛАСУ  'Hamburger'        */

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE); // замовили маленький гамбургер з сирною начинкою

// hamburger.addTopping(Hamburger.TOPPING_MAYO); // додали до гамбургера добавку з майонезу

console.log("Calories: %f", hamburger.calculateCalories()); // отримали підраховане значення калоріїв

console.log("Price: %f", hamburger.calculatePrice()); // отримали ціну замовленого гамбургера

hamburger.addTopping(Hamburger.TOPPING_SPICE); // додали до гамбургера добавку з приправ

console.log("Price with sauce: %f", hamburger.calculatePrice()); // отримали ціну з врахуванням доданої приправи

console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // перевіримо, чи великий гамбургер?

hamburger.removeTopping(Hamburger.TOPPING_SPICE); // забрали приправу з гамбургера

console.log("Have %d toppings", hamburger.getToppings().length); // повинен залишитись з добавок тільки один майонез

var h2 = new Hamburger(); // а тут не вказали при замовленні основні параметри гамбургера

var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); // а тут помилились вказавши замість розміру приправу

// додаємо багато одинакових добавок, а можна додавати тільки різні
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);

